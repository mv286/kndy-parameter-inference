% This is a function that takes the following parameters
% - Y: data.
% - X: model prediction 
% - params: structure containing the model parameters
% The function returns the distance between the data and the model
function d = model_distance(Y , X, params) 

d = sum(abs(Y-X));

