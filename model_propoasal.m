
% The function returns proposal parameter vector sampled from a multivariate normal 
% distribution with mean=thetaC and covariance matrix params.Sigma
% if iteration==1 then the iteration draws the parameter vector from the
% uniform distribution (prior) in the interval [params.minV,params.maxV]

function proposal = model_propoasal(thetaC,params, iteration) 

    if (iteration > 1)
        
        proposalTmp = mvnrnd(thetaC,  params.Sigma);
        proposal=proposalTmp';
        

    else %sample from prior distribution
        z = nan(size( params.minV));
        z(1:4) = params.minV(1:4) + (params.maxV(1:4)-params.minV(1:4)) .* rand(size(params.minV(1:4))) ;
        z(5:8) = params.minV(1:4) + (params.maxV(1:4)-params.minV(1:4)) .* rand(size(params.minV(1:4))) ;
        z(9) = params.minV(9) + (params.maxV(9)-params.minV(9)) .* rand ;
        proposal = z;
       
    end
    
   
